package tjee.lab4.ejb.minimum;

import javax.ejb.Stateless;
import java.util.List;

@Stateless
public class Minimum implements MinimumLocal {

    @Override
    public float calculate(List<Float> params) throws NullPointerException {
        if(params.isEmpty()) {
            throw new NullPointerException("Lista elementow jest pusta.");
        }
        float minimum = params.get(0);
        for(float param : params) {
            minimum = (param<minimum) ? param : minimum;
        }
        return minimum;
    }
}
