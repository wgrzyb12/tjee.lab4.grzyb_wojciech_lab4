package tjee.lab4.ejb.minimum;

import java.util.List;
import javax.ejb.Local;

@Local
public interface MinimumLocal {

    float calculate(List<Float> params) throws NullPointerException;
}
