package tjee.lab4.war.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.ws.WebServiceRef;
import tjee.lab4.war.history.access.HistoryAccess;

@WebServlet(name = "ShowHistory", urlPatterns = {"/ShowHistory"})
public class ShowHistory extends HttpServlet {

    @WebServiceRef(wsdlLocation = "WEB-INF/wsdl/localhost_8080/HistoryAccess/History.wsdl")
    private HistoryAccess HistoryAccessService;
    
    private String showHistory() {
        tjee.lab4.war.history.access.History port = HistoryAccessService.getHistoryPort();
        return port.showHistory();
    }
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Historia</title>");
            out.println("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">");
            out.println("<link rel=\"icon\" type=\"image/png\" href=\"./Style/img/favicon.png\">");
            out.println("<link rel=\"stylesheet\" href=\"./Style/css/components.css\">");
            out.println("<link rel=\"stylesheet\" href=\"./Style/css/icons.css\">");
            out.println("<link rel=\"stylesheet\" href=\"./Style/css/responsee.css\">");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Historia</h1>");
            out.println(showHistory());
            out.println("</br>");
            out.println("<a class='button rounded-btn cancel-btn s-2 margin-bottom left' href=" + request.getContextPath() + "><i class='icon-sli-arrow-left' style=\"font-size:15px\">Powrót</i></a>");
            out.println("<a class='button rounded-btn reload-btn s-2 margin-bottom right' value=\"Refresh Page\" onClick=\"window.location.reload()\"><i class='icon-refresh' style=\"font-size:15px\">Odśwież</i></a><br>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "ShowHistory - wyswietlanie historii";
    }
}