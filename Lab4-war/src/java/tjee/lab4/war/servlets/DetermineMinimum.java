package tjee.lab4.war.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.ws.WebServiceRef;
import tjee.lab4.ejb.minimum.MinimumLocal;
import tjee.lab4.war.history.access.HistoryAccess;

@WebServlet(name = "DetermineMinimum", urlPatterns = {"/DetermineMinimum"})
public class DetermineMinimum extends HttpServlet {

    @WebServiceRef(wsdlLocation = "WEB-INF/wsdl/localhost_8080/HistoryAccess/History.wsdl")
    private HistoryAccess historyAccessService;
    
    private void addResult(float result) {
        tjee.lab4.war.history.access.History port = historyAccessService.getHistoryPort();
        port.addResult(result);
    }
    
    @EJB
    private MinimumLocal minimum;
    
    private List<Float> generateParams(int n, int min, int max){
        List<Float> params = new ArrayList<>();
        Random random = new Random();
        for(int i=0; i<n; i++) {
            params.add(min + random.nextFloat()*(max - min));
        }
        return params;
    }
    
    private String paramsToString(List<Float> params) {
        String str="<table>"
                + "<tr>"
                + "<th>ID</th>"
                + "<th>Wygenerowane elementy</th>"
                + "</tr>";
        for(int i=1; i<= params.size(); i++) {
            str += "<tr>";
            str += "<td>"+i+"</td>";
            str += "<td>"+params.get(i-1)+"</td>";
            str += "</tr>";
        }
        str += "</table>";
        return str;
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Wyznaczanie najmniejszego elementu</title>");
            out.println("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">");
            out.println("<link rel=\"icon\" type=\"image/png\" href=\"./Style/img/favicon.png\">");
            out.println("<link rel=\"stylesheet\" href=\"./Style/css/components.css\">");
            out.println("<link rel=\"stylesheet\" href=\"./Style/css/icons.css\">");
            out.println("<link rel=\"stylesheet\" href=\"./Style/css/responsee.css\">");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Wyznaczanie najmniejszego elementu</h1>");
            List<Float> params = generateParams(Integer.parseInt(request.getParameter("n")),Integer.parseInt(request.getParameter("min")),Integer.parseInt(request.getParameter("max")));
            out.println(paramsToString(params));
            float min = minimum.calculate(params);
            out.println("<table>"
                    + "<tr>"
                    + "<th>Minimum</th>"
                    + "<th>"+min+"</th>"
                    + "</tr>"
                    + "</table>");
            addResult(min);
            out.println("</br>");
            out.println("<a class='button rounded-btn cancel-btn s-2 margin-bottom left' href=" + request.getContextPath() + "><i class='icon-sli-arrow-left' style=\"font-size:15px\">Powrót</i></a>");
            out.println("<a class='button rounded-btn reload-btn s-2 margin-bottom right' value=\"Refresh Page\" onClick=\"window.location.reload()\"><i class='icon-refresh' style=\"font-size:15px\">Odśwież</i></a><br>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "DetermineMinimum - wyznaczanie najmniejszego elementu";
    }
}
