package tjee.lab4.ws.history;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.jws.Oneway;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

@Startup
@Singleton
@LocalBean
@WebService(serviceName = "HistoryAccess")
public class History {
    private List<Float> history;
    
    @PostConstruct
    void init() {
        history = new ArrayList<>();
    }
    
    @WebMethod(operationName = "addResult")
    @Oneway
    public void addResult(@WebParam(name = "result") float result) {
        history.add(result);
    }
    
    @WebMethod(operationName = "showHistory")
    public String showHistory() {
        if(history.isEmpty()) {
            return "Brak danych.";
        }
        String reply = "<table>"
                + "<tr>"
                + "<th>ID</th>"
                + "<th>Wartosci minimalne</th>"
                + "</tr>";
        for(int i=1; i<= history.size(); i++) {
            reply += "<tr>";
            reply += "<td>"+i+"</td>";
            reply += "<td>"+history.get(i-1)+"</td>";
            reply += "</tr>";
        }
        reply += "</table>";
        return reply;
    }
}
